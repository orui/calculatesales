package jp.alhinc.orui_satsuki.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class CalculateSales {
	// private static final String = null;

	public static void main(String args[]){

		//コマンドライン引数の個数確認
		if (args.length != 1){
			System.out.println("予期せぬエラーが発生しました");
			return;
		}

		File dir = new File(args[0]);
		//listFileメソッドでファイルの一覧を取得
		File[] list = dir.listFiles();

		String regex = "[0-9]{3}";
		String definedName = "支店";

		List<String> saleList = new ArrayList<>();

		Map<String, String> mapCodeStore = new TreeMap<>();
		Map<String, Long> mapCodeAmount = new TreeMap<>();

		// ①（支店定義読み込みメソッドの）呼び出し
		if (!readStore(args[0], "branch.lst", regex, definedName, mapCodeStore, mapCodeAmount)) {
			// ここでのreturnは戻り値がないため処理が終了する
			return;
		}

		for (int i = 0; i < list.length; i++) {

			// fileNameは「00000001.rcd」, 「branch.lst」などファイル名を指す
			String fileName = list[i].getName();
			String fileNameN = fileName.replace(".rcd", "");
			String extension = fileName.replace(fileNameN + ".", "");

			// 8桁
			int val = String.valueOf(fileNameN).length();

			// &&extension==rcd
			if (list[i].isFile()&&(val == 8) && extension.equals("rcd") == true) {
				saleList.add(fileName);
			}
		}

		for (int f = 0; f < saleList.size() - 1; f++) {

			String fileNameNumber = (saleList.get(f).replace(".rcd", ""));
			int intFileNameNumber = Integer.parseInt(fileNameNumber);
			String comparativeNumber = (saleList.get(f + 1).replace(".rcd", ""));
			int intComparativeNumber = Integer.parseInt(comparativeNumber);

			if (intFileNameNumber + 1 != intComparativeNumber) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}

		// ②集計メソッドの呼び出し
		if (!aggregate(saleList, args[0], mapCodeStore, mapCodeAmount)) {
			return;
		}

		// ③出力メソッドの呼び出し
		// 基本形 クラス名.メソッド名(実引数)
		// 同クラス内であればクラス名.は不要
		if (!writeFile(args[0], "branch.out", mapCodeStore, mapCodeAmount)) {
			return;
		}
	}





	// ①支店定義読み込みメソッド
	private static boolean readStore(String filePath, String extensionLst, String regex, String definedName,
			Map<String, String> mapCodeStore, Map<String, Long> mapCodeAmount) {
		BufferedReader br = null;
		String line;

		try {
			File branchFile = new File(filePath, extensionLst);
			FileReader branchFileFr = new FileReader(branchFile);
			br = new BufferedReader(branchFileFr);

			while ((line = br.readLine()) != null) {
				String[] split = line.split(",");

				if (!split[0].matches(regex)) { //引数[0-9]{3}
					System.out.println(definedName+"定義ファイルのフォーマットが不正です");
					return false;
				}

				int splitLength = split.length;
				if (splitLength != 2) {
					System.out.println(definedName+"定義ファイルのフォーマットが不正です");
					return false;
				}

				mapCodeStore.put(split[0], split[1]);
				mapCodeAmount.put(split[0], 0L);
			}
		} catch (FileNotFoundException e) {
			System.out.println(definedName+"定義ファイルが存在しません");
			return false;
		} catch (IOException e) {
			// TODO 自動生成された catch ブロック
			System.out.println("予期せぬエラーが発生しました");
			return false;

			// e.printStackTrace();
			// eがエラーメッセージ表記、printStackTraceメソッドがエラーの詳細表記のメソッド

		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}

	// ②集計メソッド
	private static boolean aggregate(List<String> saleList,String filePath,Map<String, String> mapCodeStore,
			Map<String, Long> mapCodeAmount) {
		BufferedReader salesBr = null;

		for (int i = 0; i < saleList.size(); i++) {
			try {
				List<String> salesArray = new ArrayList<>();

				//String strFilePath =getPath(filePath,saleList(i));
				File salesFile = new File(filePath,saleList.get(i));
				FileReader salesFr = new FileReader(salesFile);
				salesBr = new BufferedReader(salesFr);

				String line;
				// 売上ファイル1行読み取りをlineとする
				while ((line = salesBr.readLine()) != null) {
					salesArray.add(line);
				}
				//売上ファイルが2行であるかどうかの確認
				if (salesArray.size() != 2) {
					System.out.println(saleList.get(i) + "のフォーマットが不正です");
					return false;
				}


				if (!salesArray.get(1).matches("^[0-9]+$")){
					System.out.println("予期せぬエラーが発生しました");
					return false;

				}


				//Mapに適切にkeyとvalueが入っているかの確認
				if (!mapCodeStore.containsKey(salesArray.get(0))) {
					System.out.println(saleList.get(i) + "の支店コードが不正です");
					return false;
				}
				Long sum = mapCodeAmount.get(salesArray.get(0));
				Long longAmount = Long.parseLong(salesArray.get(1));

				sum = sum + longAmount;
				String strSum = Long.toString(sum);

				if (strSum.length() > 10) {
					System.out.println("合計金額が10桁を超えました");
					return false;
				}

				mapCodeAmount.put(salesArray.get(0), sum);

			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return false;
			} finally {
				if (salesBr != null) {
					try {
						salesBr.close();
					} catch (IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return false;
					}
				}

			}
		}
		return true;
	}

	// ③出力メソッド
	private static boolean writeFile(String filePath, String extensionOut, Map<String, String> mapCodeStore,
			Map<String, Long> mapCodeAmount) {

		BufferedWriter bw = null;
		try {
			// 対象のファイルに対する書き込みを行う準備
			// 支店別集計ファイルの読み込み

			File outFile = new File(filePath, extensionOut);
			bw = new BufferedWriter(new FileWriter(outFile));

			for (Map.Entry<String, String> entry : mapCodeStore.entrySet()) {
				String codeKey = entry.getKey();
				String storeName = entry.getValue();

				// 支店別集計ファイルの書き込み
				bw.write(codeKey + "," + storeName + "," + mapCodeAmount.get(codeKey));
				bw.newLine();
			}
		} catch (IOException e) {
			// TODO 自動生成された catch ブロック
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if (bw != null) {
				try {
					bw.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}
}